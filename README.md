## TP M3206


### TP1

- [TP1](https://gitlab.com/m3206/TP/blob/master/TP1/TP1.md)


### TP2

- [TP2](https://gitlab.com/m3206/TP/blob/master/TP2/TP2.md)



### TP3

- [TP3](https://gitlab.com/m3206/TP/blob/master/TP3/TP3.md)


### Remarques 
Si vous avez des remarques (fautes, questions, découvertes, lien intéressants, ... peu importe en fait tant
que c'est lié plus ou moins ou cours) 
concernant le cours, ouvrez une issue. Cet onglet ce trouve en haut de cette page.
Vous avez la listes des onglets: `Project/Activity/.../Issues`